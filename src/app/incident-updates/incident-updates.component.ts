import { Component, OnInit, ViewChild} from '@angular/core';
import { IncidentService } from '../incident-service.service';
import { Incident } from '../incident-form/incident.model'
import { ToastrService } from 'ngx-toastr';
import { key_g } from '../../global';
import {NgxCaptchaModule,ReCaptcha2Component} from 'ngx-captcha';

@Component({
  selector: 'app-incident-updates',
  templateUrl: './incident-updates.component.html',
  styleUrls: ['./incident-updates.component.scss']
})
export class IncidentUpdatesComponent implements OnInit {
  incident:Incident = new Incident()
  updates:any;
  @ViewChild('captchaElement', {static: true} ) captchaElem: ReCaptcha2Component;
  token:any;
  public readonly siteKey = key_g
  constructor(private IncidentService: IncidentService, private toastr: ToastrService) {
    
   }

  ngOnInit() {
  }
  handleSuccess($event){
    this.token = $event;
  }
  
  getIncidentUpdates(){
    if(this.token==undefined){
      this.toastr.error("Debes ingresar el captcha","Error");
    }else{
    if(this.incident.number==undefined){
      this.toastr.error("Ingresa un numero de reclamo","Error");
      this.captchaElem.resetCaptcha();
    }else{
      console.log("aqui")
      console.log(this.token)
    this.IncidentService.getUpdates(this.incident.number,this.incident.rut,this.token).subscribe(
      result => {
        this.updates = result["incident_updates"];
        if(this.updates.length == 0 ){
          this.toastr.error("Registro no encontrado", "Revisa tu numero de reclamo");
          this.updates = []
          this.captchaElem.resetCaptcha();
        }
        return result
        },
        error =>{
          for(let step in error.error){
            this.toastr.error(error.error[step], "error")
          }   
          this.updates = []
          this.captchaElem.resetCaptcha();
          console.log("error");
        })
  }
  }
}
}
