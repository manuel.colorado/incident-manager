export class Incident{
    name?:any; 
    rut?:any; 
    channel?:any = "Falabella";
    document?:string = "Boleta";
    number_document?:number; 
    comments?:string; 
    subject?:any; 
    description?:any = "";
    last_name?:any;
    product?:any;
    document_type?:string = "RUT";
    captcha?:any;
    sale_date?: any;
    number?:any;
    email?:any;
    phone_number?:any; 
}