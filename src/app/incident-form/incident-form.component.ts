import { Incident } from './incident.model' 
import { IncidentService } from '../incident-service.service';
import { registerLocaleData } from '@angular/common';
import { key_g } from '../../global';
import { Component, OnInit, EventEmitter, ViewChild,  ElementRef, TemplateRef} from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';
import { ToastrService } from 'ngx-toastr';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {NgxCaptchaModule,ReCaptcha2Component} from 'ngx-captcha';



@Component({
  selector: 'app-incident-form',
  templateUrl: './incident-form.component.html',
  styleUrls: ['./incident-form.component.scss']
})

export class IncidentFormComponent implements OnInit {
  public readonly siteKey = key_g;
  uploadInput: EventEmitter<UploadInput>;
  dragOver: boolean;
  options: UploaderOptions;
  files: File[];
  updates: any;
  @ViewChild('captchaElement', {static: true} ) captchaElem: ReCaptcha2Component;
  @ViewChild('file_input', {static: true})
  file_input: ElementRef;
  formData: FormData;
  token:any;
  rut:any;
  maxDate: Date;
  val:File[]; 

  identidad = [
    {"name":"RUT"},
    {"name": "Pasaporte"},
    {"name": "DNI"}
  ]

  documento = [
    {"name":"Boleta"},
    {"name":"Factura"}
  ]
  canales = [
    {"name":"Presencial", "channel": 10, "value":"presencial", "image": "assets/images/presencial.png"},
    {"name": "Extra Mercadolibre", "channel": 11, "value":"extra mercadolibre", "image": "assets/images/extra_ml.png"},
    {"name": "Facebook", "channel": 14, "value":"facebook", "image": "assets/images/facebook.png"},
    {"name": "Yapo", "channel": 7, "value":"yapo", "image": "assets/images/yapo.png"},
    {"name": "Instagram", "channel": 16, "value":"instagram", "image": "assets/images/instagram.png"},
    {"name":"Mercadolibre", "channel": 1, "value":"mercadolibre", "image": "assets/images/mercadolibre.png"},
    {"name":"Falabella", "channel": 8, "value":"falabella",  "image": "assets/images/falabella.png"},
    {"name":"Groupon", "channel": 6, "value":"groupon", "image": "assets/images/groupon.png"},
    {"name": "Paris", "channel": 4, "value":"paris", "image": "assets/images/paris.png"},
    {"name": "Linio", "channel": 5, "value":"linio", "image": "assets/images/Linio_logo.svg.png"},
    {"name": "Ripley", "channel": 3, "value":"ripley", "image": "assets/images/ripley.png"}, 
    {"name": "Mercadoshop", "channel": 13, "value":"mercadoshop", "image": "assets/images/mercadoshop.png"},
    {"name": "Perdida", "channel": 15, "value":"perdida", "image": "assets/images/lost.png"},
    {"name": "Otro canal", "channel": 12, "value":"otro canal", "image": "assets/images/otro_canal.png"}
  ]

  subject = [
    {"name":"Producto Defectuoso"},
    {"name":"Faltan Piezas o Partes"},
    {"name":"Compré y no Recibí"}, 
    {"name":"Otro Problema"}
  ]

  type_identity:string;
  incident:Incident = new Incident()
  key:any = key_g;
  
  constructor(
    private IncidentService: IncidentService,
    private toastr: ToastrService,
    private localeService: BsLocaleService,
  ) { 
    this.localeService.use('es');
    this.maxDate = new Date();
    this.options = { concurrency: 1, maxUploads: 1000};
    this.files = [];
    this.val = []
    this.uploadInput = new EventEmitter<UploadInput>();
  }

  ngOnInit() {
    this.type_identity = "RUT";  
  }

  handleSuccess($event){
    this.token = $event;
  }

  checkRut(rut) {
    // Despejar Puntos
    var valor = rut.replace('.','');
    // Despejar Guión
    valor = valor.replace('-',''); 
    // Aislar Cuerpo y Dígito Verificador
    var cuerpo = valor.slice(0,-1);
    var dv = valor.slice(-1).toUpperCase();
    // Formatear RUN
    rut = cuerpo + '-'+ dv
    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if(cuerpo.length < 7) { this.toastr.error("Rut Incompleto", "Error"); return false;} 
    // Calcular Dígito Verificador
    var suma = 0;
    var multiplo = 2;
    // Para cada dígito del Cuerpo
    for(let i=1;i<=cuerpo.length;i++) {
        // Obtener su Producto con el Múltiplo Correspondiente
        var index = multiplo * valor.charAt(cuerpo.length - i);
        // Sumar al Contador General
        suma = suma + index;
        // Consolidar Múltiplo dentro del rango [2,7]
        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    }
    // Calcular Dígito Verificador en base al Módulo 11
    var dvEsperado = 11 - (suma % 11);
    // Casos Especiales (0 y K)
    dv = (dv == 'K')?10:dv;
    dv = (dv == 0)?11:dv;
    // Validar que el Cuerpo coincide con su Dígito Verificador
    if(dvEsperado != dv) { this.toastr.error("Rut Invalido", "Error"); return false; }  
    // Si todo sale bien, eliminar errores (decretar que es válido)
}
  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      const file: File = output.file.nativeFile;
      var flag = 0
      var extensions = ["pdf","jpg","png","xls","xlsx"]
      this.val.push(output.file.nativeFile) 
      console.log(this.val)
      for(let x=0; x< this.val.length; x++){
        for(let y = 0; y<extensions.length; y++){
          if(this.val[x]["name"].split(".")[1] == extensions[y]){
            flag = 1
          }
        }  
      }
      console.log(flag)
      if(flag==1){
        if(this.files.length<10){
          this.files.push(output.file.nativeFile);
        }else{
          this.toastr.error("Puedes subir un máximo de 10 archivos", "Error");
        }
      }else{
        this.toastr.error("Solo puedes subir archivos en formato XLS,JPG,WVM,PNG", "Error");
      }
      console.log(this.files)
    } 
  }

  handleFileInput(newfiles: FileList): void {
    // Future suported types: .png, .jpg
    
    for (let i = 0; i < this.files.length; i++) {
      const file: File = this.files[i];
      // make some checks for files
      this.files.push(file);
    }

  }
  removeAllFiles(): void {
    this.files = [];
    this.file_input.nativeElement.value = "";
    this.val = [];
    console.log(this.files)
    console.log(this.file_input.nativeElement.value)
  }

  registerIncident(){
    this.IncidentService.createIncident(this.incident, this.files, this.token).subscribe(
      result => {
        this.toastr.success("Reclamo cargado exitosamente, revisa tu email");
        this.incident = new Incident()
        this.removeAllFiles()
        this.captchaElem.resetCaptcha();
        return result
        },
        error =>{
          if(this.incident.rut == undefined){
            this.toastr.error("Debes ingresar un numero de documento", "Error");
          }
          for(let step in error.error){
            if (error.error[step] == "This field is required."){
              this.toastr.error("ingresa todos los campos", "Error");
            }else{
              this.toastr.error(error.error[step], "error")
            } 
          }        
          if(this.incident.phone_number == undefined){
            this.toastr.error("Debes ingresar un numero teléfonico", "Error");
          }
          if(this.incident.name == undefined){
            this.toastr.error("Debes ingresar tu nombre", "Error");
          }
          if(this.incident.description == undefined){
            this.toastr.error("Debes ingresar una descripción", "Error");
          }
          if(this.incident.description.length > 1000){
            this.toastr.error("Solo puedes ingresar 1000 caracteres en la descripción", "Error");
          }
          if(this.token == undefined){
            this.toastr.error("valida el captcha por favor", "Error");
          }
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          if(re.test(this.incident.email)){
          }else{
            this.toastr.error("Email con formato incorrecto", "error");
          }
          this.captchaElem.resetCaptcha();
        })
    
  }
}
