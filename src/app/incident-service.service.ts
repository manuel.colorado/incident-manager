import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { of, BehaviorSubject } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';
import { Observable, throwError, interval } from 'rxjs';
import { url } from '../global';
import {debounceTime} from 'rxjs/operators';
import {distinctUntilChanged} from 'rxjs/operators';
import {switchMap} from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
  })
  export class IncidentService {
    private messageSource = new BehaviorSubject('default message');
    currentMessage = this.messageSource.asObservable();
    internal_params:any={}

    public headers: HttpHeaders = new HttpHeaders();
    constructor(
         public http: HttpClient,
        ) { 
    }

    createIncident(incident:any, files:any, token:any): Observable<boolean> {
        let formData = new FormData();
        Object.keys(incident).forEach(key => {
            formData.append(key, JSON.stringify(incident[key]))
        });
        for (let i = 0; i < files.length; i++) {
          const file: File = files[i];
          formData.append('images', file, file.name);
        }
        formData.append('token',token) 
        return this.http.post(url+'incident/',formData)     
        .pipe(
            map((response: any) => {
              console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }


    getUpdates(incident_id:any, rut:any, token:any){
      return this.http.put(url + 'incident-updates/',{params:{incident_number:incident_id,rut_validation:rut, token:token},headers:this.headers})
    .pipe(
        map((res) => {
          console.log(res["id"]);
          return res;
        })
      )
    }

}