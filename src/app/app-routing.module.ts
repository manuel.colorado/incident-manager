import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentFormComponent } from './incident-form/incident-form.component';
import { IncidentUpdatesComponent } from './incident-updates/incident-updates.component';
import { esLocale } from 'ngx-bootstrap/locale';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
defineLocale('es', esLocale);

const routes: Routes = [{
  path: '',
  component: IncidentFormComponent,
},
{
  path: 'incident-updates',
  component: IncidentUpdatesComponent,
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
