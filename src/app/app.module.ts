import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastrModule } from 'ngx-toastr';
import { IncidentFormComponent } from './incident-form/incident-form.component';
import { RegisterComponent } from './incident/register/register.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxUploaderModule } from 'ngx-uploader';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { IncidentUpdatesComponent } from './incident-updates/incident-updates.component';



@NgModule({
  declarations: [
    AppComponent,
    IncidentFormComponent,
    RegisterComponent,
    IncidentUpdatesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    HttpClientModule,
    NgxUploaderModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    NgxCaptchaModule,
    NgxDatatableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
